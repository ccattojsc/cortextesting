import React, { Component } from 'react';
// import logo from "https://static.squarespace.com/static/5429cb7be4b083593c924f84/54332137e4b03b92ea4b5455/54332138e4b03b92ea4b54bb/1364601414513/1000w/CortexAPI.jpg";
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src="https://static.squarespace.com/static/5429cb7be4b083593c924f84/54332137e4b03b92ea4b5455/54332138e4b03b92ea4b54bb/1364601414513/1000w/CortexAPI.jpg" className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to Cortex Research</h1>
        </header>
        <p className="App-intro">
          We are going to try some Cortex API Calls
        </p>
      </div>
    );
  }
}

export default App;
